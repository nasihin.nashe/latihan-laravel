<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
    // dd($request->all());

    $name = $request['name'];
    $nama = $request['nama'];
    $gender = $request['gender'];
    $negara = $request['nationality'];
    $bahasa = $request['Language Spoken'];
    $bio = $request['bio'];
    return view('halaman.welcome',compact('name','nama','gender','negara','bahasa','bio'));
    }

}

